import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-dogs-list',
    templateUrl: './dogs-list.component.html',
    styleUrls: ['./dogs-list.component.css']
})
export class DogsListComponent implements OnInit {

    dogs: string[] = ['Mopsis'];
    moreDogs: string[] = ['Buldogs', 'Rotveilers', 'Pinčers', 'Terjers'];

    constructor() {
    }

    ngOnInit(): void {
    }

    addDog() {
        const dog = this.moreDogs.pop();
        this.dogs.push(dog);
    }
}
