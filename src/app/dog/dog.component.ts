import {Component, Input, OnDestroy, OnInit} from '@angular/core';

@Component({
    selector: 'app-dog',
    templateUrl: './dog.component.html',
    styleUrls: ['./dog.component.css']
})
export class DogComponent implements OnInit {

    @Input() nosaukums: string = null;

    isDisplayed: boolean = false;

    foods: string[] = [];

    constructor() {
    }

    showMore() {
        this.isDisplayed = !this.isDisplayed;
    }

    ngOnInit(): void {
        this.loadFoods();
    }

    private loadFoods() {
        this.foods = ['Zivis', 'Gaļu', 'Snickers'];
    }
}
